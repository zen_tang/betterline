﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterLine
{
    [Serializable]
    public class Class
    {
        public string Name { get; set; }
        public List<Report> Reports { get; set; }

        public Report MostRecentReport
        {
            get
            {
                var result = (from x in Reports orderby x.Date descending select x).FirstOrDefault();
                if (result != null)
                    return result;
                else
                    throw new NullReferenceException();
            }
        }

        public DateTime LastUpdated { get { return MostRecentReport.Date; } }
        public List<Assignment> Assignments { get { return MostRecentReport.Assignments; } }
        public double Average { get { return MostRecentReport.Average; } }
        public double FinalAverage { get { return MostRecentReport.FinalAverage; } }
        public string Teacher { get { return MostRecentReport.Teacher; } }

        public Class(string name)
        {
            this.Name = name;
            this.Reports = new List<Report>();
        }

        public override string ToString()
        {
            StringBuilder s=new StringBuilder(this.Name);
            s.Append("("+this.Reports.Count+")");
            foreach (var x in Reports)
            {
                s.AppendLine();
                s.AppendLine(x.Date.ToString());
            }
            return s.ToString();
        }
    }
}
