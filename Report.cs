﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterLine
{
    [Serializable]
    public class Report
    {
        private Class _class;
        public Class Class
        {
            get { return _class; }
            set
            {
                if (value != null)
                {
                    _class = value;
                    _class.Reports.Add(this);
                }
            }
        }

        public string Classname { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }

        public string Text { get; set; }

        public List<Assignment> Assignments { get; set; }
        public double Average { get; set; }
        public double FinalAverage { get; set; }

        public string Teacher { get; set; }

        public override string ToString()
        {
            return Address;
            //return Classname + " " + Date.ToShortDateString();
        }
        public override bool Equals(object obj)
        {
            if (obj is Report)
            {
                var other = obj as Report;
                return this.Date.Equals(other.Date) && this.Classname.Equals(other.Classname);
            }
            return false;
        }
        public override int GetHashCode()
        {
            return Date.GetHashCode();
        }
    }
}
