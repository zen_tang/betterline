﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using mshtml;
using System.Windows.Threading;
using System.Collections.ObjectModel;

namespace BetterLine
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string Status
        {
            get { return ""; }
            set
            {
                this.status.Text = string.Format("{0}: {1}", Stage.ToString(), value);
            }
        }

        RetrieveStage Stage = RetrieveStage.Init;

        public enum RetrieveStage
        {
            Init,
            LogIn,
            GotoReportList,
            AtReportList,
            GetReport,
            ShowReport
        }


        // -----------------------------------------------------------------------------------
        // ---------------------------------- log in ------------------------------------------

        public void LogIn()
        {
            var doc = (HTMLDocument)browser.Document;

            Status = "Logging in...";

            try
            {
                // fill in the text boxes
                var inputs = doc.getElementsByTagName("input");

                foreach (IHTMLElement x in inputs)
                {
                    if (x.id == "screenName")
                        x.setAttribute("value", getScreenName());
                    if (x.id == "password")
                        x.setAttribute("value", getPassword());
                }

                // find id is "btnSignIn" and click
                var button = doc.getElementById("btnSignIn");
                button.click();
            }
            catch
            {
                Status = "Error trying to log in.";
            }
        }

        string getPassword()
        {
            return "igtlrchs";
        }
        string getScreenName()
        {
            return "zentang";
        }


        // -----------------------------------------------------------------------------------
        // ---------------------------------- Go to report list ----------------------------

        // timer used to poll page for javascript
        private DispatcherTimer t;

        private void tryGotoReports(object sender, EventArgs e)
        {
            var doc = (HTMLDocument)browser.Document;

            IHTMLElement privateReports = doc.getElementById("menuItem12");
            if (privateReports == null)
            {
                t.Tick -= tryGotoReports;
                t.Tick += tryGotoReports;
            }
            else
            {
                t.Tick -= tryGotoReports;
                privateReports.click();
            }
        }


        // -----------------------------------------------------------------------------------
        // ------------------------------- Get addresses of reports ----------------------------


        public int getReportList(int NUM = 25)
        {
            var doc = (HTMLDocument)browser.Document;

            // this tells us what all the reports are
            var report_tags = doc.getElementsByTagName("a");

            // count how many reports there are, and store their addresses
            int found = 0;
            foreach (IHTMLElement x in report_tags)
            {
                if (x.innerText == "View")
                {
                    var href = (x as IHTMLAnchorElement).href.
                        Replace("javascript:", "").Replace("(", "").Replace(");", "");

                    var number = href.Split('\'')[1];
                    var classname = x.parentElement.parentElement.children[4].children[0].innerText;
                    var datestring = x.parentElement.parentElement.children[2].innerText;

                    Add(new Report()
                    {
                        Classname = classname,
                        Date = DateTime.Parse(datestring),
                        Address = number
                    });

                    found++;
                }
                if (found >= NUM) break;
            }

            return found;
        }

        ObservableCollection<Class> Classes { get; set; }
        ObservableCollection<Report> Reports { get; set; }

        void Add(Report r)
        {
            // always overwrite existing record, if any
            var f = Reports.FirstOrDefault(x => x.Equals(r));
            if (f != null)
            {
                Reports.Remove(f);
            }

            Reports.Add(r);

            try
            {
                // add it to the classes
                var existingClass = Classes.SingleOrDefault(_ => _.Name.Equals(r.Classname));
                if (existingClass != null)
                {
                    r.Class = existingClass;
                }
                else
                {
                    var newClass = new Class(r.Classname);
                    r.Class = newClass;

                    Classes.Add(newClass);
                }
            }
            catch (ArgumentNullException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        // -----------------------------------------------------------------------------------
        // ------------------------------- Initialize  ----------------------------        
        void browser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            switch (Stage)
            {
                case RetrieveStage.Init:
                    Stage = RetrieveStage.LogIn; break;

                case RetrieveStage.LogIn:
                    LogIn();

                    Stage = RetrieveStage.GotoReportList; break;
                case RetrieveStage.GotoReportList:

                    Status = "Navigating to private reports...";

                    // poll the page until javascript button loads
                    t.Tick += tryGotoReports;

                    Stage = RetrieveStage.AtReportList;
                    break;
                case RetrieveStage.AtReportList:

                    Status = "Collecting report data...";

                    var reportsFound = getReportList();
                    Status = reportsFound + " reports found!";

                    displayReports();
                    Stage = RetrieveStage.ShowReport;
                    break;
                case RetrieveStage.ShowReport:
                    Status = "Getting Report!!";

                    // save report
                    if (current != null)
                    {
                        var doc = (HTMLDocument)browser.Document;
                    }
                    break;
            }
        }
        string getURL(string number)
        {
            return "https://www.edline.net/DocViewBody.page?currentDocEntid=" + number;
        }

        // this is only temporary. we'll replace it with the report manager class, and we'll integrate
        // this functionality into the Report class
        Report current = null;

        private void displayReports()
        {
            myClasses.Items.Clear();
            myClasses.ItemsSource = this.Classes;

            // we want to know when the treeview selected is changed
            myClasses.SelectedItemChanged += (o, e) =>
            {
                if (myClasses.SelectedItem is Report)
                {
                    var r = myClasses.SelectedItem as Report;
                    current = r;
                    browser.Navigate(getURL(r.Address));
                }
                if (myClasses.SelectedItem is Class)
                {
                    var c = myClasses.SelectedItem as Class;
                    if (c.MostRecentReport != null)
                    {
                        current = c.MostRecentReport;
                        browser.Navigate(getURL(c.MostRecentReport.Address));
                    }
                    else
                    {
                        MessageBox.Show("Oh no!!");
                    }
                }
            };
        }

        public MainWindow()
        {
            InitializeComponent();
            //this.Closing += MainWindow_Closing;

            Classes = new ObservableCollection<Class>();
            Reports = new ObservableCollection<Report>();

            t = new DispatcherTimer();
            t.Interval = new TimeSpan(0, 0, 0, 0, 200);
            t.Start();

            browser.LoadCompleted += browser_LoadCompleted;
            Init(true);
        }

        void Init(bool online = false)
        {
            if (online)
            {
                Stage = RetrieveStage.Init;
                browser.Navigate("http://www.edline.net");
            }

            //if (Load())
            //{
            //    displayReports();
            //}
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Init(online: true);
        }
    }
}
