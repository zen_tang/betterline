﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterLine
{
    [Serializable]
    public enum Grade
    {
        A, B, C, D, F
    }

    [Serializable]
    public class Assignment
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Category { get; set; }

        public int Score { get; set; }
        public int Total { get; set; }
        public Grade Grade { get; set; }

    }
}
